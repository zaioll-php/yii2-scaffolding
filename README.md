[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](https://www.yiiframework.com/)
# Fullstack-yii2

## Instalação projeto básico fullstack-yii2

```bash
    $ composer create-project --prefer-dist zaioll/yii2-scaffolding <nome_app>
```

Para instalar o template básico do projeto, execute `$ <app_dir>/install_yii2`

O script shell `install_yii2` irá instalar o template básico do yii2 e subir os containers com `php7.2`, `nginx` e `postgresql`.

## Problemas conhecidos

* Ao tentar instalar extensões por meio da IDE, ela trava. As instalações devem ser feitas por meio do docker.
